# ocre-rs

Simple OCR service for fuzzy-searching images, Now in Rust™!

## Usage

1. Clone and `cd` into repo directory.
2. Set `TESSDATA_PREFIX` env var to your Tesseract data location, otherwise it will default to `/usr/share/tesseract/tessdata` which is not always correct.
3. (OPTIONAL) Set `PORT` env var, default is 8080.
4. `cargo run --release`
5. Done.

There is a `Dockerfile` included, so you can also fire this up with with Docker/Podman:

```
$ docker build -t ocre-rs .

[...]

$ docker run -d -p 8080:8080 ocre-rs
```

Note: currently, data files for ALL language supported by Tesseract are installed inside the container. You might wanna tweak that if you know what languages you'll be working with.

### Request

`POST` at `/ocr-request`

Note that you can omit the patterns if you only care about the full-text extraction.

```json
{
	"image": "BASE64-encoded image",
	"patterns": [
		{
			"archetype": "AAAA AAAA"
		},
		{
			"archetype": "11/11/1111"
		},
		{
			"archetype": "11/11/1111 11:11:11"
		}
	],
    // this is (optional, see a list here: https://tesseract-ocr.github.io/tessdoc/Data-Files-in-different-versions.html)
    "language": "eng "
}

```

### Response

#### 200 OK

```json
{
    "fullText": "SOME TEXT HERE\n20/12/1994 19:21:04\n20/12/1995",
    "results": [
        {
            "archetype": "AAAA AAAA",
            "matches": [
                "SOME TEXT"
            ]
        },
        {
            "archetype": "11/11/1111",
            "matches": [
                "20/12/1994",
                "20/12/1995"
            ]
        },
        {
            "archetype": "11/11/1111 11:11:11",
            "matches": [
                "20/12/1994 19:21:04"
            ]
        }
    ]
}
```

#### 400 Bad Request, 500 Internal Server Error

```json
{
    "code": 400,
    "message": "error message"
}
```

# License

(C) 2022 - Ariela Wenner

This project is licensed under the GNU AGPLv3 or later. See the [license](COPYING.md) for details.