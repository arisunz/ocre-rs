FROM rust:slim

WORKDIR /usr/share/ocre-rs

RUN apt-get update && apt-get upgrade -y && apt-get install -y pkg-config clang libleptonica-dev libtesseract-dev tesseract-ocr tesseract-ocr-all

ADD src src
ADD Cargo.lock Cargo.lock
ADD Cargo.toml Cargo.toml

RUN cargo build --release

ENV TESSDATA_PREFIX /usr/share/tesseract-ocr/4.00/tessdata/

ENTRYPOINT [ "target/release/ocre-rs" ]