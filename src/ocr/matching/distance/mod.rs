/// get the Hamming distance between s1 and s2
fn distance_from(s1: &str, s2: &str) -> usize {
    let mut total = 0;
    s1.chars().zip(s2.chars()).for_each(|(c1, c2)| {
        if c1 != c2 {
            total += 1;
        }
    });

    total
}

/// name says it all, sort the strings in matches by
/// their distance to archetype, in descending order
pub fn sort_by_closest(archetype: &str, matches: &[String]) -> Vec<String> {
    let mut matches_copy: Vec<String> = matches.to_owned();
    matches_copy.sort_by(|a, b| {
        let d1 = distance_from(archetype, a);
        let d2 = distance_from(archetype, b);

        d1.cmp(&d2)
    });

    matches_copy
}

#[cfg(test)]
mod test;
