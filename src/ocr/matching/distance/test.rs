use super::{distance_from, sort_by_closest};

#[test]
fn distance_is_zero() {
    let d = distance_from("aaa", "aaa");
    assert_eq!(0, d);
}

#[test]
fn distance_is_correct() {
    let d = distance_from("aaa", "abb");
    assert_eq!(2, d);
}

#[test]
fn correct_sorting() {
    let archetype = String::from("aaaa");
    let matches: Vec<String> = vec!["abbb", "aaab", "aaaa", "aabb"]
        .iter()
        .map(|s| s.to_string())
        .collect();

    let expected: Vec<String> = vec!["aaaa", "aaab", "aabb", "abbb"]
        .iter()
        .map(|s| s.to_string())
        .collect();

    let sorted = sort_by_closest(&archetype, &matches);
    for (s1, s2) in expected.iter().zip(sorted.iter()) {
        assert_eq!(s1, s2);
    }
}
