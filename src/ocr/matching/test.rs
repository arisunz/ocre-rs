use super::ArchetypeQuery;
use super::PatternField::*;

const FULL_TEXT: &str = "foo #bar _12baz5";

#[test]
fn pattern_fields_length() {
    let q = ArchetypeQuery::new(FULL_TEXT);

    assert_eq!(FULL_TEXT.len(), q.fields.len());
}

#[test]
fn pattern_matches_archetype() {
    let q = ArchetypeQuery::new(FULL_TEXT);

    let expected_fields = vec![
        Letter, Letter, Letter, Literal, // "foo "
        Literal, Letter, Letter, Letter, Literal, // "#bar "
        Literal, Digit, Digit, Letter, Letter, Letter, Digit, // "_12baz5"
    ];

    q.fields
        .iter()
        .zip(expected_fields.iter())
        .for_each(|(p1, p2)| assert_eq!(p1, p2));
}

#[test]
fn can_find_query() {
    let archetype = "#bar _12baz5".to_string();
    let query = ArchetypeQuery::new(&archetype);

    let matches = query.all_matches(FULL_TEXT).unwrap();
    assert_eq!(1, matches.len());
    assert_eq!(archetype, *matches.get(0).unwrap());
}
