use regex::Regex;

mod distance;

const FUZZY_MATCHER: &str = "[\\d\\p{L}]";

#[derive(Debug, PartialEq, Eq)]
enum PatternField {
    Digit,
    Letter,
    Literal,
}

pub struct ArchetypeQuery {
    pub archetype: String,
    fields: Vec<PatternField>,
}

#[derive(Debug)]
pub struct MatcherError(String);

impl From<regex::Error> for MatcherError {
    fn from(e: regex::Error) -> Self {
        MatcherError(e.to_string())
    }
}

fn pattern_fields_from_archetype(archetype: &str) -> Vec<PatternField> {
    archetype
        .chars()
        .map(|c| {
            if c.is_alphabetic() {
                PatternField::Letter
            } else if c.is_numeric() {
                PatternField::Digit
            } else {
                PatternField::Literal
            }
        })
        .collect()
}

impl ArchetypeQuery {
    pub fn new(archetype: &str) -> Self {
        ArchetypeQuery {
            archetype: archetype.to_string(),
            fields: pattern_fields_from_archetype(archetype),
        }
    }

    /// generate a fuzzy regex pattern that matches any string that resembles self.archetype
    fn fuzzy_pattern(&self) -> String {
        self.fields
            .iter()
            .zip(self.archetype.chars())
            .map(|(f, c)| match f {
                PatternField::Digit => FUZZY_MATCHER.to_string(),
                PatternField::Letter => FUZZY_MATCHER.to_string(),
                PatternField::Literal => format!(r#"{c}"#), // aa
            })
            .collect::<String>()
    }

    /// get all matches for self.archetype in full_text
    pub fn all_matches(&self, full_text: &str) -> Result<Vec<String>, MatcherError> {
        let pattern = self.fuzzy_pattern();
        let re = Regex::new(&pattern)?;

        // i want this to be DONE. i would like this to be FINISHED.
        let whatever: Vec<String> = re
            .captures_iter(full_text)
            .flat_map(|x| {
                x.iter()
                    .filter(|x| x.is_some())
                    .map(|x| x.unwrap().as_str().to_string())
                    .collect::<Vec<String>>()
            })
            .collect();

        Ok(distance::sort_by_closest(&self.archetype, &whatever))
    }
}

#[cfg(test)]
mod test;
