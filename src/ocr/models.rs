use axum::Json;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Deserialize)]
pub struct QueryRequest {
    pub image: String,
    pub patterns: Vec<Pattern>,
    pub language: Option<String>,
}

#[derive(Deserialize)]
pub struct Pattern {
    pub archetype: String,
}

#[derive(Serialize)]
pub struct SingleQueryResult {
    pub archetype: String,
    pub matches: Vec<String>,
}

#[derive(Serialize)]
pub struct QueryResponse {
    pub full_text: Option<String>,
    pub results: Option<Vec<SingleQueryResult>>,
}

pub struct ErrorResponse {
    code: u16,
    message: Option<String>,
}

impl QueryResponse {
    pub fn empty() -> Self {
        Self {
            full_text: None,
            results: None,
        }
    }
}

impl From<QueryResponse> for Json<Value> {
    fn from(q: QueryResponse) -> Self {
        Json(json!({
            "fullText": q.full_text,
            "results": q.results
        }))
    }
}

impl ErrorResponse {
    pub fn new(code: u16, message: Option<String>) -> Self {
        Self { code, message }
    }
}

impl From<ErrorResponse> for Json<Value> {
    fn from(e: ErrorResponse) -> Self {
        Json(json!({
            "code": e.code,
            "message": e.message
        }))
    }
}
