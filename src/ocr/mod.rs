mod matching;
mod models;

use std::{fmt::Display, str::Utf8Error};

use axum::{http::StatusCode, Json};
use leptess::{leptonica::PixError, tesseract::TessInitError, LepTess};

use models::*;
use serde_json::Value;

use self::matching::ArchetypeQuery;

#[derive(Debug)]
enum OcrError {
    InitError,
    ReadError,
}

impl Display for OcrError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InitError => write!(f, "error initializing OCR engine"),
            Self::ReadError => write!(f, "error reading data from image"),
        }
    }
}

impl From<TessInitError> for OcrError {
    fn from(_: TessInitError) -> Self {
        OcrError::InitError
    }
}

impl From<Utf8Error> for OcrError {
    fn from(_: Utf8Error) -> Self {
        OcrError::ReadError
    }
}

impl From<PixError> for OcrError {
    fn from(_: PixError) -> Self {
        OcrError::ReadError
    }
}

fn read_text(img: &[u8], lang: &str) -> Result<String, OcrError> {
    // use tessdata directory from TESSDATA_PREFIX env var
    // no use in even attempting to come up with a default value
    let mut lt = LepTess::new(None, lang)?;
    lt.set_image_from_mem(img)?;

    Ok(lt.get_utf8_text()?)
}

pub async fn process_ocr_query(Json(request): Json<QueryRequest>) -> (StatusCode, Json<Value>) {
    tracing::info!("handling new request");

    let image = match base64::decode(request.image) {
        Ok(img) => img,
        Err(_) => {
            return (
                StatusCode::BAD_REQUEST,
                ErrorResponse::new(
                    StatusCode::BAD_REQUEST.as_u16(),
                    Some("not a valid image".to_string()),
                )
                .into(),
            )
        }
    };

    // make sure we actually get an image
    let mime = match http_types::Mime::sniff(image.as_slice()) {
        Ok(m) => m,
        Err(_) => {
            return (
                StatusCode::BAD_REQUEST,
                ErrorResponse::new(
                    StatusCode::BAD_REQUEST.as_u16(),
                    Some("not a valid image".to_string()),
                )
                .into(),
            )
        }
    };

    match mime.basetype() {
        "image" => (),
        _ => {
            return (
                StatusCode::BAD_REQUEST,
                ErrorResponse::new(
                    StatusCode::BAD_REQUEST.as_u16(),
                    Some("not a valid image".to_string()),
                )
                .into(),
            )
        }
    };

    // TODO: extract most of the stuff below so we can write some proper tests
    // TODO: alternatively, leave this here but write integration tests

    let lang = request.language.unwrap_or_else(|| "eng".to_string());
    let ocr_result = read_text(image.as_slice(), &lang);
    let full_text = match ocr_result {
        Ok(text) => text,
        Err(err) => match err {
            // the client probably fucked up
            OcrError::InitError => return (
                StatusCode::BAD_REQUEST,
                ErrorResponse::new(
                    StatusCode::BAD_REQUEST.as_u16(),
                    Some("could not initialize OCR engine, invalid language or misconfigured server?".to_string())).into()),

            // we probably fucked up
            OcrError::ReadError => {
                return (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    ErrorResponse::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), Some("error reading text from image".to_string())).into(),
                )
            }
        },
    };

    // extract queries from the request...
    let queries: Vec<ArchetypeQuery> = request
        .patterns
        .iter()
        .map(|p| ArchetypeQuery::new(&p.archetype))
        .collect();

    // ...and match them against the full text
    let mut query_results: Vec<SingleQueryResult> = Vec::new();
    queries
        .iter()
        .map(|q| match q.all_matches(&full_text) {
            Ok(matches) => Some(SingleQueryResult {
                archetype: q.archetype.clone(),
                matches,
            }),
            Err(_) => None,
        })
        .for_each(|res| {
            if let Some(x) = res {
                if !x.matches.is_empty() {
                    query_results.push(x);
                }
            }
        });

    tracing::info!("sending response with {} matches", query_results.len());

    (
        StatusCode::OK,
        QueryResponse {
            full_text: Some(full_text),
            results: Some(query_results),
        }
        .into(),
    )
}

#[cfg(test)]
mod test;
