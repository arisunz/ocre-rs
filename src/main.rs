use std::net::SocketAddr;

use axum::{routing::post, Router};
use ocr::process_ocr_query;

mod ocr;

const DEFAULT_PORT: u16 = 8080;

fn port_or_default() -> u16 {
    match std::env::var("PORT") {
        Ok(p) => match p.parse::<u16>() {
            Ok(p) => p,
            Err(_) => DEFAULT_PORT,
        },
        Err(_) => DEFAULT_PORT,
    }
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let app = Router::new().route("/ocr-request", post(process_ocr_query));

    let addr = SocketAddr::from(([0, 0, 0, 0], port_or_default()));

    tracing::info!("Running on {}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
